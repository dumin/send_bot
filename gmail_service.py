import pickle
import os.path
import os
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from email.mime.text import MIMEText
import base64
from pprint import pprint
from datetime import datetime

SCOPES = ['https://www.googleapis.com/auth/gmail.send']
USER_ID='me'


def main():
    creds = None
    with open('dir.txt', 'w') as f:
        f.writelines([i for i in os.listdir()])
    # Check existing creds
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            

            flow = InstalledAppFlow.from_client_secrets_file('credentials.json', 
                                                             SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

            # creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('gmail', 'v1', credentials=creds, cache_discovery=False)
    return service

def create_message(sender, to, subject, message_text):
  message = MIMEText(message_text)
  message['to'] = to
  message['from'] = sender
  message['subject'] = subject
  print(message)
  print()
  raw_message = base64.urlsafe_b64encode(message.as_string().encode("utf-8"))
  return {
    'raw': raw_message.decode("utf-8")
  }
  

def send_message(service, message):
  try:
    message = service.users().messages().send(userId=USER_ID, body=message).execute()
    print(f'----Email was sent at {datetime.now()}----')
    print(f'----Session ended at {datetime.now()}----')
    return message
  except Exception as e:
    print('An error occurred: %s' % e)
    return None

if __name__ == '__main__':
    main()