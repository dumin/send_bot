import re
from datetime import datetime

import telebot
from telebot import types

import gmail_service
from BOT_TOKEN import TOKEN
from messages import *
import os
import logging

# Logger config
logging.basicConfig(filename="bot.log", level=logging.DEBUG, 
                    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

# Basic variables
bot = telebot.TeleBot(TOKEN)
#regex = re.compile("^[w\-\.]+@([w\-\]+.)+[w\-\]{2,4}$")
email_address = ''

# Markups 
cancel_send_markup = types.ReplyKeyboardMarkup(one_time_keyboard=True).add('/cancel', '/send')
start_markup = types.ReplyKeyboardMarkup(one_time_keyboard=True).add('/start')
cancel_markup = types.ReplyKeyboardMarkup(one_time_keyboard=True).add('/cancel')

# Reset email var if /cancel command was received
def reset_globals():
    global email_address
    email_address = ''

# Email validation
def check_email(email):
    # if re.match(regex, email):
    #     return True
    # else:
    #     return False

    return True

# Bot 
@bot.message_handler(commands=['start'])
def send_welcome(message):
    if message.text == '/edit':
        bot.send_message(message.chat.id, '`Enter new text`')
        bot.register_next_step_handler(message.text, edit_message)
    else:
        reset_globals()
        bot.send_message(message.chat.id, WELCOME_MESSAGE, parse_mode='markdown')
        bot.register_next_step_handler(message, validate)
        

@bot.message_handler(func=lambda message: True)
def validate(message): 
    if message.text in ['/cancel', '/start']:
        send_welcome(message)
    elif message.text == '/edit':
        bot.send_message(chat_id=message.chat.id, 
                         text='`Enter new text or click Cancel to start over`', 
                         parse_mode='markdown', 
                         reply_markup=cancel_markup)
        bot.register_next_step_handler(message, edit_message)
    else:
        global email_address
        email_address = message.text.lower() 
        print(f'----Email address {email_address} received at {datetime.now()}')       
        if email_address.count('@') == 1:
            if check_email(email_address):
                bot.send_message(chat_id=message.chat.id, 
                                 text='`Click Send to send an email or Cancel to reset`', 
                                 reply_markup=cancel_send_markup, 
                                 parse_mode='markdown')       
                bot.register_next_step_handler(message, send_or_cancel)
            else:
                bot.send_message(chat_id=message.chat.id, reply_to_message_id=message.message_id,
                                 text='`The email is incorrect`',
                                 parse_mode='markdown')
                send_welcome(message)
        else:
            bot.send_message(chat_id=message.chat.id, reply_to_message_id=message.message_id,
                             text='`The message does not contain an email address`',
                             parse_mode='markdown')
            send_welcome(message)

            
@bot.message_handler(commands=['/send', '/cancel'])
def send_or_cancel(message):
    global email_address
    if message.text == '/send':
        service = gmail_service.main()
        with open('message_text.txt') as f:
            email_text = ''.join(f.readlines())
        email = gmail_service.create_message(SENDER, email_address, SUBJECT, email_text)
        gmail_service.send_message(service, email)
        bot.send_message(chat_id=message.chat.id, 
                         text='`Emails will be sent soon`', 
                         parse_mode='markdown',    
                         reply_markup=start_markup)
        with open('log.txt', 'a') as file:
            file.write(f'{message.from_user.id:<15} {email_address:<30} {str(datetime.today()):<30}\n')
    elif message.text == '/cancel':
        send_welcome(message)


@bot.message_handler(commands=['/edit'])
def edit_message(message):
    if message.text == '/cancel':
        send_welcome(message)
    else:
        with open('message_text.txt', 'w') as f:
            f.write(message.text)
        bot.send_message(chat_id=message.chat.id,
                         text='`Text was saved`', 
                         parse_mode='markdown')
        send_welcome(message)

print(f'Service started at {datetime.now()}')
# print(os.listdir())

while True:
    try:
        logging.info("Bot running..")
        bot.polling(none_stop=True, interval=2)
        break
    except telebot.apihelper.ApiException as e:
        logging.error(e)
        bot.stop_polling()
        time.sleep(15)
        logging.info("Running again!")
